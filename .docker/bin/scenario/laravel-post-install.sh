# Fill the variables #
#var_dir="/Users/planet17/Works/ak/api"
# Check whether exists parent of provided directory
if [ -f .env ]; then
    cat .env > .env.tmp;
    cat .env.tmp | sed -e '/^#/d;/^\s*$/d' -e "s/'/'\\\''/g" -e "s/=\(.*\)/='\1'/g" > .env.tmp2
    set -a; source .env.tmp2; set +a
    rm .env.tmp .env.tmp2
fi

dir_var_parent_str_pos_end=${var_dir%/*}
dir_var_parent=${var_dir:0:${#dir_var_parent_str_pos_end}}
# Create whether not exists.
if [[ ! -f $dir_var_parent ]]
  then
    mkdir -p $dir_var_parent
fi
# Move installed project to target directory
mv -i installed $var_dir
if [ "$?" -ne "0" ]; then
  echo "\033[0;31mmv failed"
  exit 1
fi
# Move additional files for Docker
cp  -R ./.docker/bin/scenario/docker-files/php/.docker $var_dir/.docker
cp ./.docker/bin/scenario/docker-files/php/docker-compose.yml $var_dir/docker-compose.yml
# Move additional files for Environment
var_env=$var_dir/.env
var_env_template=$var_dir/.env.example
if [ ! -f "$var_env" ]; then
    if [ -f "$var_env_template" ]; then
        cp $var_env_template var_env
    else
        touch var_env
    fi
fi
