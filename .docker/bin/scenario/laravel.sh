# Set up variables
#var_installation_dir="installed"
#composer_container_name_installer="installer"
if [ -f .env ]; then
    cat .env > .env.tmp;
    cat .env.tmp | sed -e '/^#/d;/^\s*$/d' -e "s/'/'\\\''/g" -e "s/=\(.*\)/='\1'/g" > .env.tmp2
    set -a; source .env.tmp2; set +a
    rm .env.tmp .env.tmp2
fi

path_to_package_inside_vendor="vendor/laravel/installer/bin/laravel"
# Install packages required for installation
docker-compose exec $composer_container_name_installer composer require laravel/installer --update-with-all-dependencies
docker-compose exec $composer_container_name_installer composer global require laravel/installer --update-with-all-dependencies
# Remove previous installed project whether exists
if [ -f "$var_installation_dir" ]; then
    docker-compose exec $composer_container_name_installer rm -rf $var_installation_dir
fi
# Run installation process
docker-compose exec $composer_container_name_installer $path_to_package_inside_vendor new $var_installation_dir
