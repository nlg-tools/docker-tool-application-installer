docker-compose down
rm ./.docker/logs/php.log
touch ./.docker/logs/php.log
docker-compose build
#  --force-recreate
docker-compose up -d
docker-compose exec installer composer dump-autoload --optimize
docker-compose exec installer composer require laravel/installer
docker-compose exec installer composer require roave/security-advisories
docker-compose exec installer composer update
docker ps
