# Project with preinstalled libraries, Composer and some other features #

Whether you have any installed tools, but want to create a some project based on
some frameworks.

It could be uses for installation some project from following frameworks: `Laravel`.
(other are coming soon)...

## Requirements: ##

    Docker

## Using: ##

    At first make your own .env from .env.dist
    At second must be filled some variable in the .env name and pathes where project will be moved on. 

### Important ###

    *) Just fill scenario - write custom or use exist from (`./.docker/bin/scenario`) to the `./.docker/bin/app-install.sh`.
    *) Don't forget to change PHP version at `.docker/images/php/Dockerfile` and change correct case for `Section: Install some base extensions`.

#### Then can use following script: ####

```shell script
./.docker/bin/docker-restart.sh ## install docker
./.docker/bin/app-install.sh ## call selected scenario
```
